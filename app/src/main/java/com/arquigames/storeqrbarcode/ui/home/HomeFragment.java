package com.arquigames.storeqrbarcode.ui.home;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.arquigames.storeqrbarcode.R;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        Button btn = root.findViewById(R.id.home_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView myImageView = (ImageView) root.findViewById(R.id.home_imgview);
                Bitmap myBitmap = BitmapFactory.decodeResource(
                        HomeFragment.this.getActivity().getResources(),
                        R.drawable.barcode);
                myImageView.setImageBitmap(myBitmap);


                BarcodeDetector detector =
                        new BarcodeDetector.Builder(HomeFragment.this.getActivity())
                                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE | Barcode.CODABAR)
                                .build();
                if(!detector.isOperational()){
                    textView.setText("Could not set up the detector!");
                    return;
                }

                Frame frame = new Frame.Builder().setBitmap(myBitmap).build();
                SparseArray<Barcode> barcodes = detector.detect(frame);
                if(barcodes.size() > 0){
                    Barcode thisCode = barcodes.valueAt(0);
                    textView.setText(thisCode.rawValue);
                }else{
                    textView.setText("Empty barcodes values");
                }
            }
        });

        return root;
    }
}